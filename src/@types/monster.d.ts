type MonsterType = () => number;
type MonsterTypeVoid = () => void;

export type Monster = {
  name: string;
  level: number;
  HP: number;
  ConstHP: number;
  costOfKilling: number;
  imageUrl: string;
  time: number;
  countCostOfKilling(): MonsterType;
  increaseLevel(): MonsterTypeVoid;
  decreaseLevelByTimeOver(): MonsterTypeVoid;
  increaseHP(): MonsterTypeVoid;
  increaseCostOfKilling(): MonsterTypeVoid;
  drawNewMob(): MonsterTypeVoid;
};
