type UserType = () => number;

export type User = {
  name: string;
  damage: number;
  coins: number;
  weaponsInstance: any;
  weapons: any;
  weaponBought(): UserType;
  weaponUpgrade(): UserType;
  calculateDamage(): UserType;
};
