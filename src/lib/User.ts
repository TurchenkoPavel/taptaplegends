import Weapons from '@/lib/Weapons'
import { interfaceWeapon } from '@/lib/interfaces'
import { UserInterface } from '@/interfaces/UserInterface'

export default class User implements UserInterface {
  name: string;
  damage: number;
  coins: number;
  weaponsInstance: any;
  weapons: any;
  timerID: number;
  timeout: number;
  constructor () {
    this.name = 'anonymous'
    this.damage = 1
    this.coins = 0
    this.weaponsInstance = new Weapons()
    this.weapons = this.weaponsInstance.weapons
    this.timerID = 0
    this.timeout = 1000
  }

  weaponBought (weapon: any) {
    this.weapons.map((item: interfaceWeapon) => {
      if (item.name === weapon.name) {
        this.coins -= item.cost
        item.isBought = true
        item.level++
        item.cost = weapon.calculateCost()
        item.previousDamage = item.increaseDamage
        item.increaseDamage = weapon.calculateDamage()
      }

      return item
    })
    this.damage = this.calculateDamage()
  }

  weaponUpgrade (weapon: any) {
    this.weapons.map((item: interfaceWeapon) => {
      if (item.name === weapon.name) {
        this.coins -= item.cost
        item.level++
        item.cost = weapon.calculateCost()
        item.previousDamage = item.increaseDamage
        item.increaseDamage = weapon.calculateDamage()
      }
      return item
    })

    this.damage = this.calculateDamage()
  }

  calculateDamage () :number {
    return 1 + this.weapons.reduce((acc: number, item: interfaceWeapon) => {
      if (item.isBought) {
        acc += item.previousDamage
      }
      return acc
    }, 0)
  }

  timerForHit (hitFunction: any): number {
    return setInterval(() => {
      hitFunction.call()
    }, this.timeout)
  }
}
