import { MonsterInterface } from '@/interfaces'

export default class Monster implements MonsterInterface {
  name: string;
  level: number;
  HP: number;
  ConstHP: number;
  costOfKilling: number;
  ConstCostOfKilling: number;
  imageUrl: string;
  time: number;
  startHPonLevel: number;
  timerId: number;
  timer: string;
  constructor () {
    this.name = 'Monster'
    this.level = 1
    this.HP = 10
    this.ConstHP = 10
    this.costOfKilling = 2
    this.ConstCostOfKilling = 2
    this.startHPonLevel = 10
    this.imageUrl = 'first.jpg'
    this.time = 30
    this.timerId = 0
    this.timer = '00:30'
  }

  countCostOfKilling () : number {
    return this.costOfKilling * this.level
  }

  increaseLevel (): void {
    this.level++
    this.increaseHP(this.level)
    this.increaseCostOfKilling(this.level)
    this.drawNewMob()
    this.restartTimer()
  }

  decreaseLevelByTimeOver (): void {
    this.level = (this.level > 10) ? this.level -= 10 : 1
    const changedLevel = this.level === 1 ? 0 : this.level
    this.increaseHP(changedLevel)
    this.increaseCostOfKilling(changedLevel)
    this.drawNewMob()
  }

  increaseHP (level: number): void {
    const coefficient = (1.5 * level === 0) ? 1 : 1.5 * level
    this.HP = this.startHPonLevel = Number((this.ConstHP + this.ConstHP * coefficient).toFixed(0))
  }

  increaseCostOfKilling (level: number): void {
    const coefficient = (1.25 * level === 0) ? 1 : 1.25 * level
    this.costOfKilling = Number((this.ConstCostOfKilling + this.ConstCostOfKilling * coefficient).toFixed(0))
  }

  drawNewMob (): void {
    this.imageUrl = (this.imageUrl === 'first.jpg') ? 'second.png' : 'first.jpg'
  }

  timerForKill (): void {
    let time = this.time
    this.timerId = setInterval(() => {
      time -= 1
      if (time < 0) {
        time = this.time
        this.decreaseLevelByTimeOver()
        this.restartTimer()
      }
      this.timer = (time < 10) ? `00:0${time}` : `00:${time}`
    }, 1000)
  }

  restartTimer () : void {
    clearInterval(this.timerId)
    this.timerForKill()
  }
}
