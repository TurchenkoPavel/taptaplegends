import { HelpersInterface } from '@/interfaces'

export default class Helpers implements HelpersInterface {
  countValue (value: number): string {
    if (value < 1000) {
      return (value).toFixed(2)
    }
    if (value >= 1000 && value < 1000000) {
      return (value / 1000).toFixed(2) + 'K'
    }
    if (value >= 1000000 && value < 1000000000) {
      return (value / 1000000).toFixed(2) + 'M'
    }
    if (value >= 1000000000 && value < 1000000000000) {
      return (value / 1000000000).toFixed(2) + 'B'
    }
    if (value >= 1000000000000 && value < 1000000000000000) {
      return (value / 1000000000000).toFixed(2) + 'T'
    }
    const ordA = 'a'.charCodeAt(0)
    const ordZ = 'z'.charCodeAt(0)
    const len = ordZ - ordA + 1

    let s = ''
    while (value >= 0) {
      s = String.fromCharCode(value % len + ordA) + s
      value = Math.floor(value / len) - 1
    }
    return s
  }
}
