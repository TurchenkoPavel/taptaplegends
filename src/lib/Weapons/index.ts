// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export const Branch = {
  name: 'Branch',
  cost: 10,
  level: 0,
  increaseDamage: 50,
  increaseByLevel: 1.3
}
export const StoneAxe = {
  name: 'StoneAxe',
  cost: 200,
  level: 0,
  increaseDamage: 250,
  increaseByLevel: 1.7
}
export const Sling = {
  name: 'Sling',
  cost: 500,
  level: 0,
  increaseDamage: 500,
  increaseByLevel: 2.2
}
export const Bow = {
  name: 'Bow',
  cost: 750,
  level: 0,
  increaseDamage: 1000,
  increaseByLevel: 2.5
}
export const Spear = {
  name: 'Spear',
  cost: 1000,
  level: 0,
  increaseDamage: 1500,
  increaseByLevel: 2.9
}
export const Mace = {
  name: 'Mace',
  cost: 1500,
  level: 0,
  increaseDamage: 2000,
  increaseByLevel: 3.3
}
export const Hammer = {
  name: 'Hammer',
  cost: 2000,
  level: 0,
  increaseDamage: 2500,
  increaseByLevel: 4
}
