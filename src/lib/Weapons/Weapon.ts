interface DestructedOptions {
  name: string;
  cost: number;
  level: number;
  increaseDamage: number;
  increaseByLevel: number;
}
export default class Weapon {
  name: string;
  defaultCost: number;
  defaultDamage: number;
  level: number;
  cost: number;
  increaseDamage: number;
  increaseByLevel: number;
  isBought: boolean;
  previousDamage: number;

  constructor (opts: DestructedOptions) {
    this.name = opts.name
    this.defaultCost = opts.cost
    this.defaultDamage = opts.increaseDamage
    this.level = opts.level
    this.cost = this.calculateCost()
    this.increaseDamage = this.calculateDamage()
    this.increaseByLevel = opts.increaseByLevel
    this.isBought = false
    this.previousDamage = opts.increaseDamage
  }

  calculateCost (): number {
    if (this.level > 0) {
      return Number((this.defaultCost * this.level * this.increaseByLevel).toFixed(0))
    }
    return this.defaultCost
  }

  calculateDamage (): number {
    if (this.level > 0) {
      return Number((this.previousDamage * this.increaseByLevel).toFixed(0))
    }
    return this.defaultDamage
  }
}
