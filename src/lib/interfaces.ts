type MyFunctionType = () => number;

export interface interfaceWeapon {
  name: string;
  cost: number;
  level: number;
  increaseDamage: number;
  previousDamage: number;
  isBought: boolean;
  defaultCost: number;
  defaultDamage: number;
  calculateCost(): MyFunctionType;
  calculateDamage(): MyFunctionType;
}
