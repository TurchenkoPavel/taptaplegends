import Weapon from '@/lib/Weapons/Weapon'
import * as WeaponsList from '@/lib/Weapons/index'

interface interfaceWeaponsList {
  [key: string]: any;
}

const dependencyWeaponsList: interfaceWeaponsList = WeaponsList

export default class Weapons {
  weapons: any;
  weaponsName: Array<string>;
  constructor () {
    this.weapons = []
    this.weaponsName = Object.keys(dependencyWeaponsList)
    this.generateWeapons()
  }

  generateWeapons () {
    this.weaponsName.forEach(item => {
      this.weapons.push(new Weapon(dependencyWeaponsList[item]))
    })
  }
}
