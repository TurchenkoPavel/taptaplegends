import { createStore, createLogger } from 'vuex'
import createPersistedState from 'vuex-persistedstate'

// TODO: How to surpass cyclical dependency linting errors cleanly?
// eslint-disable-next-line import/no-cycle
import { store as user, UserStore, State as UserState } from '@/store/modules/user'
import { store as monster, MonsterStore, State as MonsterState } from '@/store/modules/monster'

export type RootState = {
  user: UserState;
  monster: MonsterState;
};

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export type Store = UserStore<Pick<RootState, 'user'>> & MonsterStore<Pick<RootState, 'monster'>>;

// Plug in logger when in development environment
const debug = process.env.NODE_ENV !== 'production'
const plugins = debug ? [createLogger({})] : []

// Plug in session storage based persistence
plugins.push(createPersistedState({ storage: window.sessionStorage }))

export const store = createStore({
  plugins,
  modules: {
    user,
    monster
  }
})

export function useStore (): Store {
  return store as Store
}
