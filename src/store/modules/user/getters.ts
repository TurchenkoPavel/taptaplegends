import { GetterTree } from 'vuex'

import type { User } from '@/@types'

// eslint-disable-next-line import/no-cycle
import { RootState } from '@/store'

import { State } from './state'

export type Getters = {
  getUser(state: State): User
}

export const getters: GetterTree<State, RootState> & Getters = {
  getUser: (state) => state.user
}
