import { ActionTree, ActionContext } from 'vuex'

// eslint-disable-next-line import/no-cycle
import { RootState } from '@/store'

import { State } from './state'
import { Mutations } from './mutations'
import { UserActionTypes } from './action-types'

type AugmentedActionContext = {
  commit<K extends keyof Mutations>(
    key: K,
    payload: Parameters<Mutations[K]>[1],
  ): ReturnType<Mutations[K]>;
} & Omit<ActionContext<State, RootState>, 'commit'>

export interface Actions {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  [UserActionTypes.GET_USER](
    { commit }: AugmentedActionContext,
    someId: string, // Obsolete in here but left as an example
  ): Promise<boolean>;
}

export const actions: ActionTree<State, RootState> & Actions = {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  async [UserActionTypes.GET_USER] ({ commit }, someId: string) {
    console.log(someId)
  }
}
