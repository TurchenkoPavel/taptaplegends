import { MutationTree } from 'vuex'

import { User } from '@/@types'

import { State } from './state'
import { UserMutationTypes } from './mutation-types'

export type Mutations<S = State> = {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  [UserMutationTypes.SET_USER](state: S, payload: User): void;
}

export const mutations: MutationTree<State> & Mutations = {
  [UserMutationTypes.SET_USER] (state: State, user: User) {
    state.user = user
  }
}
