import { GetterTree } from 'vuex'

import type { Monster } from '@/@types'

// eslint-disable-next-line import/no-cycle
import { RootState } from '@/store'

import { State } from './state'

export type Getters = {
  getMonster(state: State): Monster
}

export const getters: GetterTree<State, RootState> & Getters = {
  getMonster: (state) => state.monster
}
