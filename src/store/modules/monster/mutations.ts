import { MutationTree } from 'vuex'

import { Monster } from '@/@types'

import { State } from './state'
import { MonsterMutationTypes } from './mutation-types'

export type Mutations<S = State> = {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  [MonsterMutationTypes.SET_MONSTER](state: S, payload: Monster): void;
}

export const mutations: MutationTree<State> & Mutations = {
  [MonsterMutationTypes.SET_MONSTER] (state: State, monster: Monster) {
    state.monster = monster
  }
}
