import { Monster } from '@/@types'

export type State = {
  monster: Monster | any;
}

export const state: State = {
  monster: null
}
