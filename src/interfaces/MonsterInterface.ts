export interface MonsterInterface {
  name: string;
  level: number;
  HP: number;
  ConstHP: number;
  costOfKilling: number;
  imageUrl: string;
  time: number;
  ConstCostOfKilling: number;
  startHPonLevel: number;
  timerId: number;
  timer: string;

  countCostOfKilling(): number;
  increaseLevel(): void;
  decreaseLevelByTimeOver(): void;
  increaseHP(value: number): void;
  increaseCostOfKilling(value: number): void;
  drawNewMob(): void;
  timerForKill(): void;
  restartTimer(): void;
}
