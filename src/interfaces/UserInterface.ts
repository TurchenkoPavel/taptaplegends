export interface UserInterface {
  name: string;
  damage: number;
  coins: number;
  weaponsInstance: any;
  weapons: any;

  weaponBought(weapon: any): void;
  weaponUpgrade(weapon: any): void;
  calculateDamage(): number;
  timerForHit(hitFunction: any): number;
}
